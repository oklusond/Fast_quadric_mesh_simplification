#region License
/*
MIT License

Copyright(c) 2017 Mattias Edlund

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#endregion

#region Original License
/////////////////////////////////////////////
//
// Mesh Simplification Tutorial
//
// (C) by Sven Forstmann in 2014
//
// License : MIT
// http://opensource.org/licenses/MIT
//
//https://github.com/sp4cerat/Fast-Quadric-Mesh-Simplification
#endregion

using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityMeshSimplifier
{
	/// <summary>
	/// The mesh simplifier.
	/// Deeply based on https://github.com/sp4cerat/Fast-Quadric-Mesh-Simplification but rewritten completely in C#.
	/// </summary>
	public sealed class MeshSimplifier
	{
		#region Consts
		private const double kDoubleEpsilon = 1.0E-3;
		private const int kUVChannelCount = 4;
		#endregion

		#region Classes
		#region Triangle
		private struct Triangle
		{
			#region Fields
			public int v0;
			public int v1;
			public int v2;
			public int subMeshIndex;

			public int va0;
			public int va1;
			public int va2;

			public double err0;
			public double err1;
			public double err2;
			public double err3;

			public bool deleted;
			public bool dirty;
			public Vector3d n;
			#endregion

			#region Properties
			public int this[int index]
			{
				get
				{
					return (index == 0 ? v0 : (index == 1 ? v1 : v2));
				}
				set
				{
					switch (index)
					{
						case 0:
							v0 = value;
							break;
						case 1:
							v1 = value;
							break;
						case 2:
							v2 = value;
							break;
						default:
							throw new IndexOutOfRangeException();
					}
				}
			}
			#endregion

			#region Constructor
			public Triangle(int v0, int v1, int v2, int subMeshIndex)
			{
				this.v0 = v0;
				this.v1 = v1;
				this.v2 = v2;
				this.subMeshIndex = subMeshIndex;

				this.va0 = v0;
				this.va1 = v1;
				this.va2 = v2;

				err0 = err1 = err2 = err3 = 0;
				deleted = dirty = false;
				n = new Vector3d();
			}
			#endregion

			#region Public Methods
			public void GetAttributeIndices(int[] attributeIndices)
			{
				attributeIndices[0] = va0;
				attributeIndices[1] = va1;
				attributeIndices[2] = va2;
			}

			public void SetAttributeIndex(int index, int value)
			{
				switch (index)
				{
					case 0:
						va0 = value;
						break;
					case 1:
						va1 = value;
						break;
					case 2:
						va2 = value;
						break;
					default:
						throw new IndexOutOfRangeException();
				}
			}

			public void GetErrors(double[] err)
			{
				err[0] = err0;
				err[1] = err1;
				err[2] = err2;
			}
			#endregion
		}
		#endregion

		#region Vertex
		private struct Vertex
		{
			/// <summary>
			/// Vertex position
			/// </summary>
			public Vector3d p;
			/// <summary>
			/// First triangle index referenced by this vertex
			/// </summary>
			public int tstart;
			/// <summary>
			/// Count of triangles referencing this vertex
			/// </summary>
			public int tcount;
			public SymmetricMatrix q;
			/// <summary>
			/// Is border vertex
			/// </summary>
			public bool border;

			public Vertex(Vector3d p)
			{
				this.p = p;
				this.tstart = 0;
				this.tcount = 0;
				this.q = new SymmetricMatrix();
				this.border = true;
			}
		}
		#endregion

		#region Ref
		private struct Ref
		{
			public int tid;
			public int tvertex;

			public void Set(int tid, int tvertex)
			{
				this.tid = tid;
				this.tvertex = tvertex;
			}
		}
		#endregion

		#region UV Channels
		private class UVChannels<TVec>
		{
			private ResizableArray<TVec>[] channels = null;
			private TVec[][] channelsData = null;

			public TVec[][] Data
			{
				get
				{
					for (int i = 0; i < kUVChannelCount; i++)
					{
						if (channels[i] != null)
						{
							channelsData[i] = channels[i].Data;
						}
						else
						{
							channelsData[i] = null;
						}
					}
					return channelsData;
				}
			}

			/// <summary>
			/// Gets or sets a specific channel by index.
			/// </summary>
			/// <param name="index">The channel index.</param>
			public ResizableArray<TVec> this[int index]
			{
				get { return channels[index]; }
				set { channels[index] = value; }
			}

			public UVChannels()
			{
				channels = new ResizableArray<TVec>[kUVChannelCount];
				channelsData = new TVec[kUVChannelCount][];
			}

			/// <summary>
			/// Resizes all channels at once.
			/// </summary>
			/// <param name="capacity">The new capacity.</param>
			/// <param name="trimExess">If exess memory should be trimmed.</param>
			public void Resize(int capacity, bool trimExess = false)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					if (channels[i] != null)
					{
						channels[i].Resize(capacity, trimExess);
					}
				}
			}
		}
		#endregion

		#region Border Vertex
		private struct BorderVertex
		{
			public int index;
			public int hash;

			public BorderVertex(int index, int hash)
			{
				this.index = index;
				this.hash = hash;
			}
		}
		#endregion

		#region Border Vertex Comparer
		private class BorderVertexComparer : IComparer<BorderVertex>
		{
			public static readonly BorderVertexComparer instance = new BorderVertexComparer();

			public int Compare(BorderVertex x, BorderVertex y)
			{
				return x.hash.CompareTo(y.hash);
			}
		}
		#endregion
		#endregion

		#region Fields
		private bool _preserveBorders = false;
		private bool _enableSmartLink = true;
		private int _maxIterationCount = 100;
		private double _agressiveness = 7.0;
		private bool _verbose = false;

		private float _vertexLinkDistance = 0.0001f;

		private int _subMeshCount = 0;
		private int[] _subMeshOffsets = null;
		private ResizableArray<Triangle> _triangles = null;
		private ResizableArray<Vertex> _vertices = null;
		private ResizableArray<Ref> _refs = null;

		private ResizableArray<Vector3> _vertNormals = null;
		private ResizableArray<Vector4> _vertTangents = null;
		private UVChannels<Vector2> _vertUV2D = null;
		private UVChannels<Vector3> _vertUV3D = null;
		private UVChannels<Vector4> _vertUV4D = null;
		private ResizableArray<Color> _vertColors = null;
		private ResizableArray<BoneWeight> _vertBoneWeights = null;

		private Matrix4x4[] _bindposes = null;

		// Pre-allocated buffers
		private double[] _errArr = new double[3];
		private int[] _attributeIndexArr = new int[3];
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets if borders should be preserved.
		/// Default value: false
		/// </summary>
		public bool PreserveBorders
		{
			get { return _preserveBorders; }
			set { _preserveBorders = value; }
		}

		/// <summary>
		/// Helps to process meshes without topology
		/// Default value: true
		/// </summary>
		public bool EnableSmartLink
		{
			get { return _enableSmartLink; }
			set { _enableSmartLink = value; }
		}

		/// <summary>
		/// Gets or sets the maximum iteration count. Higher number is more expensive but can bring you closer to your target quality.
		/// Sometimes a lower maximum count might be desired in order to lower the performance cost.
		/// Default value: 100
		/// </summary>
		public int MaxIterationCount
		{
			get { return _maxIterationCount; }
			set { _maxIterationCount = value; }
		}

		/// <summary>
		/// Gets or sets the agressiveness of the mesh simplification. Higher number equals higher quality, but more expensive to run.
		/// Default value: 7.0
		/// </summary>
		public double Agressiveness
		{
			get { return _agressiveness; }
			set { _agressiveness = value; }
		}

		/// <summary>
		/// Gets or sets if verbose information should be printed to the console.
		/// Default value: false
		/// </summary>
		public bool Verbose
		{
			get { return _verbose; }
			set { _verbose = value; }
		}


		public float VertexLinkDistance
		{
			get { return _vertexLinkDistance; }
			set { _vertexLinkDistance = value; }
		}

		/// <summary>
		/// Gets or sets the vertex positions.
		/// </summary>
		public Vector3[] Vertices
		{
			get
			{
				int vertexCount = this._vertices.Length;
				var vertices = new Vector3[vertexCount];
				var vertArr = this._vertices.Data;
				for (int i = 0; i < vertexCount; i++)
				{
					vertices[i] = (Vector3)vertArr[i].p;
				}
				return vertices;
			}
			set
			{
				if (value == null)
					throw new ArgumentNullException("value");

				_bindposes = null;
				_vertices.Resize(value.Length);
				var vertArr = _vertices.Data;
				for (int i = 0; i < value.Length; i++)
				{
					vertArr[i] = new Vertex(value[i]);
				}
			}
		}

		/// <summary>
		/// Gets the count of sub-meshes.
		/// </summary>
		public int SubMeshCount
		{
			get { return _subMeshCount; }
		}

		/// <summary>
		/// Gets or sets the vertex normals.
		/// </summary>
		public Vector3[] Normals
		{
			get { return (_vertNormals != null ? _vertNormals.Data : null); }
			set
			{
				InitializeVertexAttribute(value, ref _vertNormals, "normals");
			}
		}

		/// <summary>
		/// Gets or sets the vertex tangents.
		/// </summary>
		public Vector4[] Tangents
		{
			get { return (_vertTangents != null ? _vertTangents.Data : null); }
			set
			{
				InitializeVertexAttribute(value, ref _vertTangents, "tangents");
			}
		}

		/// <summary>
		/// Gets or sets the vertex UV set 1.
		/// </summary>
		public Vector2[] UV1
		{
			get { return GetUVs2D(0); }
			set { SetUVs(0, value); }
		}

		/// <summary>
		/// Gets or sets the vertex UV set 2.
		/// </summary>
		public Vector2[] UV2
		{
			get { return GetUVs2D(1); }
			set { SetUVs(1, value); }
		}

		/// <summary>
		/// Gets or sets the vertex UV set 3.
		/// </summary>
		public Vector2[] UV3
		{
			get { return GetUVs2D(2); }
			set { SetUVs(2, value); }
		}

		/// <summary>
		/// Gets or sets the vertex UV set 4.
		/// </summary>
		public Vector2[] UV4
		{
			get { return GetUVs2D(3); }
			set { SetUVs(3, value); }
		}

		/// <summary>
		/// Gets or sets the vertex colors.
		/// </summary>
		public Color[] Colors
		{
			get { return (_vertColors != null ? _vertColors.Data : null); }
			set
			{
				InitializeVertexAttribute(value, ref _vertColors, "colors");
			}
		}

		/// <summary>
		/// Gets or sets the vertex bone weights.
		/// </summary>
		public BoneWeight[] BoneWeights
		{
			get { return (_vertBoneWeights != null ? _vertBoneWeights.Data : null); }
			set
			{
				InitializeVertexAttribute(value, ref _vertBoneWeights, "boneWeights");
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Creates a new mesh simplifier.
		/// </summary>
		public MeshSimplifier()
		{
			_triangles = new ResizableArray<Triangle>(0);
			_vertices = new ResizableArray<Vertex>(0);
			_refs = new ResizableArray<Ref>(0);
		}

		/// <summary>
		/// Creates a new mesh simplifier.
		/// </summary>
		/// <param name="mesh">The original mesh to simplify.</param>
		public MeshSimplifier(Mesh mesh)
		{
			if (mesh == null)
				throw new ArgumentNullException("mesh");

			_triangles = new ResizableArray<Triangle>(0);
			_vertices = new ResizableArray<Vertex>(0);
			_refs = new ResizableArray<Ref>(0);

			Initialize(mesh);
		}
		#endregion

		#region Private Methods
		#region Initialize Vertex Attribute
		private void InitializeVertexAttribute<T>(T[] attributeValues, ref ResizableArray<T> attributeArray, string attributeName)
		{
			if (attributeValues != null && attributeValues.Length == _vertices.Length)
			{
				if (attributeArray == null)
				{
					attributeArray = new ResizableArray<T>(attributeValues.Length, attributeValues.Length);
				}
				else
				{
					attributeArray.Resize(attributeValues.Length);
				}

				var arrayData = attributeArray.Data;
				Array.Copy(attributeValues, 0, arrayData, 0, attributeValues.Length);
			}
			else
			{
				if (attributeValues != null && attributeValues.Length > 0)
				{
					Debug.LogErrorFormat("Failed to set vertex attribute '{0}' with {1} length of array, when {2} was needed.", attributeName, attributeValues.Length, _vertices.Length);
				}
				attributeArray = null;
			}
		}
		#endregion

		#region Calculate Error
		private double VertexError(ref SymmetricMatrix q, double x, double y, double z)
		{
			return q.m0 * x * x + 2 * q.m1 * x * y + 2 * q.m2 * x * z + 2 * q.m3 * x + q.m4 * y * y
				+ 2 * q.m5 * y * z + 2 * q.m6 * y + q.m7 * z * z + 2 * q.m8 * z + q.m9;
		}

		private double CalculateError(ref Vertex vert0, ref Vertex vert1, out Vector3d result, out int resultIndex)
		{
			// compute interpolated vertex
			SymmetricMatrix q = (vert0.q + vert1.q);
			bool border = (vert0.border & vert1.border);
			double error = 0.0;
			double det = q.Determinant1();
			if (det != 0.0 && !border)
			{
				// q_delta is invertible
				result = new Vector3d(
					-1.0 / det * q.Determinant2(),  // vx = A41/det(q_delta)
					1.0 / det * q.Determinant3(),   // vy = A42/det(q_delta)
					-1.0 / det * q.Determinant4()); // vz = A43/det(q_delta)
				error = VertexError(ref q, result.x, result.y, result.z);
				resultIndex = 2;
			}
			else
			{
				// det = 0 -> try to find best result
				Vector3d p1 = vert0.p;
				Vector3d p2 = vert1.p;
				Vector3d p3 = (p1 + p2) * 0.5f;
				double error1 = VertexError(ref q, p1.x, p1.y, p1.z);
				double error2 = VertexError(ref q, p2.x, p2.y, p2.z);
				double error3 = VertexError(ref q, p3.x, p3.y, p3.z);
				error = MathHelper.Min(error1, error2, error3);
				if (error == error3)
				{
					result = p3;
					resultIndex = 2;
				}
				else if (error == error2)
				{
					result = p2;
					resultIndex = 1;
				}
				else if (error == error1)
				{
					result = p1;
					resultIndex = 0;
				}
				else
				{
					result = p3;
					resultIndex = 2;
				}
			}
			return error;
		}
		#endregion

		#region Flipped
		/// <summary>
		/// Check if a triangle flips when this edge is removed
		/// </summary>
		private bool Flipped(ref Vector3d p, int i0, int i1, ref Vertex v0, bool[] deleted)
		{
			int tcount = v0.tcount;
			var refs = this._refs.Data;
			var triangles = this._triangles.Data;
			var vertices = this._vertices.Data;
			for (int k = 0; k < tcount; k++)
			{
				Ref r = refs[v0.tstart + k];
				if (triangles[r.tid].deleted)
					continue;

				int s = r.tvertex;
				int id1 = triangles[r.tid][(s + 1) % 3];
				int id2 = triangles[r.tid][(s + 2) % 3];
				if (id1 == i1 || id2 == i1)
				{
					deleted[k] = true;
					continue;
				}

				Vector3d d1 = vertices[id1].p - p;
				d1.Normalize();
				Vector3d d2 = vertices[id2].p - p;
				d2.Normalize();
				double dot = Vector3d.Dot(ref d1, ref d2);
				if (System.Math.Abs(dot) > 0.999)
					return true;

				Vector3d n;
				Vector3d.Cross(ref d1, ref d2, out n);
				n.Normalize();
				deleted[k] = false;
				dot = Vector3d.Dot(ref n, ref triangles[r.tid].n);
				if (dot < 0.2)
					return true;
			}

			return false;
		}
		#endregion

		#region Update Triangles
		/// <summary>
		/// Update triangle connections and edge error after a edge is collapsed.
		/// </summary>
		private void UpdateTriangles(int i0, int ia0, ref Vertex v, ResizableArray<bool> deleted, ref int deletedTriangles)
		{
			Vector3d p;
			int pIndex;
			int tcount = v.tcount;
			var triangles = this._triangles.Data;
			var vertices = this._vertices.Data;
			for (int k = 0; k < tcount; k++)
			{
				Ref r = _refs[v.tstart + k];
				int tid = r.tid;
				Triangle t = triangles[tid];
				if (t.deleted)
					continue;

				if (deleted[k])
				{
					triangles[tid].deleted = true;
					++deletedTriangles;
					continue;
				}

				t[r.tvertex] = i0;
				if (ia0 != -1)
				{
					t.SetAttributeIndex(r.tvertex, ia0);
				}

				t.dirty = true;
				t.err0 = CalculateError(ref vertices[t.v0], ref vertices[t.v1], out p, out pIndex);
				t.err1 = CalculateError(ref vertices[t.v1], ref vertices[t.v2], out p, out pIndex);
				t.err2 = CalculateError(ref vertices[t.v2], ref vertices[t.v0], out p, out pIndex);
				t.err3 = MathHelper.Min(t.err0, t.err1, t.err2);
				triangles[tid] = t;
				_refs.Add(r);
			}
		}
		#endregion

		#region Move/Merge Vertex Attributes
		private void MoveVertexAttributes(int i0, int i1)
		{
			if (_vertNormals != null)
			{
				_vertNormals[i0] = _vertNormals[i1];
			}
			if (_vertTangents != null)
			{
				_vertTangents[i0] = _vertTangents[i1];
			}
			if (_vertUV2D != null)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					var vertUV = _vertUV2D[i];
					if (vertUV != null)
					{
						vertUV[i0] = vertUV[i1];
					}
				}
			}
			if (_vertUV3D != null)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					var vertUV = _vertUV3D[i];
					if (vertUV != null)
					{
						vertUV[i0] = vertUV[i1];
					}
				}
			}
			if (_vertUV4D != null)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					var vertUV = _vertUV4D[i];
					if (vertUV != null)
					{
						vertUV[i0] = vertUV[i1];
					}
				}
			}
			if (_vertColors != null)
			{
				_vertColors[i0] = _vertColors[i1];
			}
			if (_vertBoneWeights != null)
			{
				_vertBoneWeights[i0] = _vertBoneWeights[i1];
			}
		}

		private void MergeVertexAttributes(int i0, int i1)
		{
			if (_vertNormals != null)
			{
				_vertNormals[i0] = (_vertNormals[i0] + _vertNormals[i1]) * 0.5f;
			}
			if (_vertTangents != null)
			{
				_vertTangents[i0] = (_vertTangents[i0] + _vertTangents[i1]) * 0.5f;
			}
			if (_vertUV2D != null)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					var vertUV = _vertUV2D[i];
					if (vertUV != null)
					{
						vertUV[i0] = (vertUV[i0] + vertUV[i1]) * 0.5f;
					}
				}
			}
			if (_vertUV3D != null)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					var vertUV = _vertUV3D[i];
					if (vertUV != null)
					{
						vertUV[i0] = (vertUV[i0] + vertUV[i1]) * 0.5f;
					}
				}
			}
			if (_vertUV4D != null)
			{
				for (int i = 0; i < kUVChannelCount; i++)
				{
					var vertUV = _vertUV4D[i];
					if (vertUV != null)
					{
						vertUV[i0] = (vertUV[i0] + vertUV[i1]) * 0.5f;
					}
				}
			}
			if (_vertColors != null)
			{
				_vertColors[i0] = (_vertColors[i0] + _vertColors[i1]) * 0.5f;
			}

			// TODO: Do we have to blend bone weights at all or can we just keep them as it is in this scenario?
		}
		#endregion

		#region Are UVs The Same
		private bool AreUVsTheSame(int channel, int indexA, int indexB)
		{
			if (_vertUV2D != null)
			{
				var vertUV = _vertUV2D[channel];
				if (vertUV != null)
				{
					var uvA = vertUV[indexA];
					var uvB = vertUV[indexB];
					return uvA == uvB;
				}
			}

			if (_vertUV3D != null)
			{
				var vertUV = _vertUV3D[channel];
				if (vertUV != null)
				{
					var uvA = vertUV[indexA];
					var uvB = vertUV[indexB];
					return uvA == uvB;
				}
			}

			if (_vertUV4D != null)
			{
				var vertUV = _vertUV4D[channel];
				if (vertUV != null)
				{
					var uvA = vertUV[indexA];
					var uvB = vertUV[indexB];
					return uvA == uvB;
				}
			}

			return false;
		}
		#endregion

		#region Remove Vertex Pass
		/// <summary>
		/// Remove vertices and mark deleted triangles
		/// </summary>
		private void RemoveVertexPass(int startTrisCount, int targetTrisCount, double threshold, ResizableArray<bool> deleted0, ResizableArray<bool> deleted1, ref int deletedTris)
		{
			Triangle[] triangles = _triangles.Data;
			int triangleCount = _triangles.Length;
			Vertex[] vertices = _vertices.Data;

			Vector3d p;
			int pIndex;
			for (int tid = 0; tid < triangleCount; tid++)
			{
				if (triangles[tid].dirty || triangles[tid].deleted || triangles[tid].err3 > threshold)
					continue;

				triangles[tid].GetErrors(_errArr);
				triangles[tid].GetAttributeIndices(_attributeIndexArr);
				for (int edgeIndex = 0; edgeIndex < 3; edgeIndex++)
				{
					if (_errArr[edgeIndex] > threshold)
						continue;

					int nextEdgeIndex = ((edgeIndex + 1) % 3);
					int i0 = triangles[tid][edgeIndex];
					int i1 = triangles[tid][nextEdgeIndex];

					// Border check
					if (vertices[i0].border != vertices[i1].border)
						continue;
					// If borders should be preserved
					else if (_preserveBorders && vertices[i0].border)
						continue;

					// Compute vertex to collapse to
					CalculateError(ref vertices[i0], ref vertices[i1], out p, out pIndex);
					deleted0.Resize(vertices[i0].tcount); // normals temporarily
					deleted1.Resize(vertices[i1].tcount); // normals temporarily

					// Don't remove if flipped
					if (Flipped(ref p, i0, i1, ref vertices[i0], deleted0.Data))
						continue;
					if (Flipped(ref p, i1, i0, ref vertices[i1], deleted1.Data))
						continue;

					int ia0 = _attributeIndexArr[edgeIndex];

					// Not flipped, so remove edge
					vertices[i0].p = p;
					vertices[i0].q += vertices[i1].q;

					if (pIndex == 1)
					{
						// Move vertex attributes from ia1 to ia0
						int ia1 = _attributeIndexArr[nextEdgeIndex];
						MoveVertexAttributes(ia0, ia1);
					}
					else if (pIndex == 2)
					{
						// Merge vertex attributes ia0 and ia1 into ia0
						int ia1 = _attributeIndexArr[nextEdgeIndex];
						MergeVertexAttributes(ia0, ia1);
					}

					int tstart = _refs.Length;
					UpdateTriangles(i0, ia0, ref vertices[i0], deleted0, ref deletedTris);
					UpdateTriangles(i0, ia0, ref vertices[i1], deleted1, ref deletedTris);

					int tcount = _refs.Length - tstart;
					if (tcount <= vertices[i0].tcount)
					{
						// save ram
						if (tcount > 0)
						{
							var refsArr = _refs.Data;
							Array.Copy(refsArr, tstart, refsArr, vertices[i0].tstart, tcount);
						}
					}
					else
					{
						// append
						vertices[i0].tstart = tstart;
					}

					vertices[i0].tcount = tcount;
					break;
				}

				// Check if we are already done
				if ((startTrisCount - deletedTris) <= targetTrisCount)
					break;
			}
		}
		#endregion

		#region Update Mesh
		/// <summary>
		/// Compact triangles, compute edge error and build reference list.
		/// </summary>
		/// <param name="iteration">The iteration index.</param>
		private void UpdateMesh(int iteration)
		{
			Triangle[] triangles = _triangles.Data;
			Vertex[] vertices = _vertices.Data;

			int triangleCount = _triangles.Length;
			int vertexCount = _vertices.Length;
			if (iteration > 0) // compact triangles
			{
				int dst = 0;
				for (int i = 0; i < triangleCount; i++)
				{
					if (!triangles[i].deleted)
					{
						if (dst != i)
						{
							triangles[dst] = triangles[i];
						}
						dst++;
					}
				}
				this._triangles.Resize(dst);
				triangles = this._triangles.Data;
				triangleCount = dst;
			}

			UpdateReferences();

			// Identify boundary : vertices[].border=0,1
			if (iteration == 0)
			{
				Ref[] refs = _refs.Data;
				List<int> vcount = new List<int>(8);
				List<int> vids = new List<int>(8);
				int vsize = 0;
				for (int i = 0; i < vertexCount; i++)
				{
					// Init Quadrics by Plane & Edge Errors
					//
					// required at the beginning ( iteration == 0 )
					// recomputing during the simplification is not required,
					// but mostly improves the result for closed meshes
					vertices[i].q = new SymmetricMatrix();
					vertices[i].border = false;
				}

				if (_enableSmartLink)
				{
					Dictionary<Vector3Int, int> vertexRemapping = new Dictionary<Vector3Int, int>();
					float oneOverEpsilon = 1.0f / _vertexLinkDistance;

					for (int i = 0; i < vertexCount; i++)
					{
						Vector3d point = vertices[i].p;
						Vector3Int pointBucket = new Vector3Int(Mathf.FloorToInt(((float)point.x) * oneOverEpsilon), Mathf.FloorToInt(((float)point.y) * oneOverEpsilon), Mathf.FloorToInt(((float)point.z) * oneOverEpsilon));
						int correspondingVertexIndex;
						if (vertexRemapping.TryGetValue(pointBucket, out correspondingVertexIndex))
						{
							int tstart = vertices[i].tstart;
							int tcount = vertices[i].tcount;
							for (int j = tstart; j < tstart + tcount; ++j)
							{
								Ref r = refs[j];
								triangles[r.tid][r.tvertex] = correspondingVertexIndex;
								triangles[r.tid].dirty = true; 
							}
						}
						else
						{
							vertexRemapping[pointBucket] = i;
						}
					}

					// Update the references again
					UpdateReferences();
				}

				int ofs;
				int id;

				//For each vertex and find border vertices
				//Border vertex is that one referenced just once as starting or ending vertex of an edge
				for (int i = 0; i < vertexCount; i++)
				{
					int tstart = vertices[i].tstart;
					int tcount = vertices[i].tcount;
					vcount.Clear();
					vids.Clear();
					vsize = 0;

					//Go through all triangles referencing this vertex
					for (int j = 0; j < tcount; j++)
					{
						int tid = refs[tstart + j].tid;
						//For each vertex from that triangle
						for (int k = 0; k < 3; k++)
						{
							ofs = 0;
							id = triangles[tid][k];
							while (ofs < vsize)
							{
								if (vids[ofs] == id)
									break;

								++ofs;
							}

							if (ofs == vsize)
							{
								vcount.Add(1);
								vids.Add(id);
								++vsize;
							}
							else
							{
								++vcount[ofs];
							}
						}
					}

					for (int j = 0; j < vsize; j++)
					{
						if (vcount[j] == 1)
						{
							id = vids[j];
							vertices[id].border = true;
						}
					}
				}

				int v0, v1, v2;
				Vector3d n, p0, p1, p2, p10, p20, dummy;
				int dummy2;
				SymmetricMatrix sm;
				for (int i = 0; i < triangleCount; i++)
				{
					v0 = triangles[i].v0;
					v1 = triangles[i].v1;
					v2 = triangles[i].v2;

					p0 = vertices[v0].p;
					p1 = vertices[v1].p;
					p2 = vertices[v2].p;
					p10 = p1 - p0;
					p20 = p2 - p0;
					Vector3d.Cross(ref p10, ref p20, out n);
					n.Normalize();
					triangles[i].n = n;

					sm = new SymmetricMatrix(n.x, n.y, n.z, -Vector3d.Dot(ref n, ref p0));
					vertices[v0].q += sm;
					vertices[v1].q += sm;
					vertices[v2].q += sm;
				}

				for (int i = 0; i < triangleCount; i++)
				{
					// Calc Edge Error
					var triangle = triangles[i];
					triangles[i].err0 = CalculateError(ref vertices[triangle.v0], ref vertices[triangle.v1], out dummy, out dummy2);
					triangles[i].err1 = CalculateError(ref vertices[triangle.v1], ref vertices[triangle.v2], out dummy, out dummy2);
					triangles[i].err2 = CalculateError(ref vertices[triangle.v2], ref vertices[triangle.v0], out dummy, out dummy2);
					triangles[i].err3 = MathHelper.Min(triangles[i].err0, triangles[i].err1, triangles[i].err2);
				}
			}
		}
		#endregion

		#region Update References
		private void UpdateReferences()
		{
			int triangleCount = _triangles.Length;
			int vertexCount = _vertices.Length;
			Triangle[] triangles = _triangles.Data;
			Vertex[] vertices = _vertices.Data;

			// Init Reference ID list
			for (int i = 0; i < vertexCount; i++)
			{
				vertices[i].tstart = 0;
				vertices[i].tcount = 0;
			}

			for (int i = 0; i < triangleCount; i++)
			{
				++vertices[triangles[i].v0].tcount;
				++vertices[triangles[i].v1].tcount;
				++vertices[triangles[i].v2].tcount;
			}

			int tstart = 0;
			for (int i = 0; i < vertexCount; i++)
			{
				vertices[i].tstart = tstart;
				tstart += vertices[i].tcount;
				vertices[i].tcount = 0;
			}

			// Write References
			_refs.Resize(tstart);
			Ref[] refs = _refs.Data;
			for (int i = 0; i < triangleCount; i++)
			{
				int v0 = triangles[i].v0;
				int v1 = triangles[i].v1;
				int v2 = triangles[i].v2;
				int start0 = vertices[v0].tstart;
				int count0 = vertices[v0].tcount;
				int start1 = vertices[v1].tstart;
				int count1 = vertices[v1].tcount;
				int start2 = vertices[v2].tstart;
				int count2 = vertices[v2].tcount;

				refs[start0 + count0].Set(i, 0);
				refs[start1 + count1].Set(i, 1);
				refs[start2 + count2].Set(i, 2);

				++vertices[v0].tcount;
				++vertices[v1].tcount;
				++vertices[v2].tcount;
			}
		}
		#endregion

		#region Compact Mesh
		/// <summary>
		/// Finally compact mesh before exiting.
		/// </summary>
		private void CompactMesh()
		{
			int dst = 0;
			var vertices = this._vertices.Data;
			int vertexCount = this._vertices.Length;
			for (int i = 0; i < vertexCount; i++)
			{
				vertices[i].tcount = 0;
			}

			var vertNormals = (this._vertNormals != null ? this._vertNormals.Data : null);
			var vertTangents = (this._vertTangents != null ? this._vertTangents.Data : null);
			var vertUV2D = (this._vertUV2D != null ? this._vertUV2D.Data : null);
			var vertUV3D = (this._vertUV3D != null ? this._vertUV3D.Data : null);
			var vertUV4D = (this._vertUV4D != null ? this._vertUV4D.Data : null);
			var vertColors = (this._vertColors != null ? this._vertColors.Data : null);
			var vertBoneWeights = (this._vertBoneWeights != null ? this._vertBoneWeights.Data : null);

			int lastSubMeshIndex = -1;
			_subMeshOffsets = new int[_subMeshCount];

			var triangles = this._triangles.Data;
			int triangleCount = this._triangles.Length;
			for (int i = 0; i < triangleCount; i++)
			{
				var triangle = triangles[i];
				if (!triangle.deleted)
				{
					if (triangle.va0 != triangle.v0)
					{
						int iDest = triangle.va0;
						int iSrc = triangle.v0;
						vertices[iDest].p = vertices[iSrc].p;
						if (vertBoneWeights != null)
						{
							vertBoneWeights[iDest] = vertBoneWeights[iSrc];
						}
						triangle.v0 = triangle.va0;
					}
					if (triangle.va1 != triangle.v1)
					{
						int iDest = triangle.va1;
						int iSrc = triangle.v1;
						vertices[iDest].p = vertices[iSrc].p;
						if (vertBoneWeights != null)
						{
							vertBoneWeights[iDest] = vertBoneWeights[iSrc];
						}
						triangle.v1 = triangle.va1;
					}
					if (triangle.va2 != triangle.v2)
					{
						int iDest = triangle.va2;
						int iSrc = triangle.v2;
						vertices[iDest].p = vertices[iSrc].p;
						if (vertBoneWeights != null)
						{
							vertBoneWeights[iDest] = vertBoneWeights[iSrc];
						}
						triangle.v2 = triangle.va2;
					}
					int newTriangleIndex = dst++;
					triangles[newTriangleIndex] = triangle;

					vertices[triangle.v0].tcount = 1;
					vertices[triangle.v1].tcount = 1;
					vertices[triangle.v2].tcount = 1;

					if (triangle.subMeshIndex > lastSubMeshIndex)
					{
						for (int j = lastSubMeshIndex + 1; j < triangle.subMeshIndex; j++)
						{
							_subMeshOffsets[j] = newTriangleIndex;
						}
						_subMeshOffsets[triangle.subMeshIndex] = newTriangleIndex;
						lastSubMeshIndex = triangle.subMeshIndex;
					}
				}
			}

			triangleCount = dst;
			for (int i = lastSubMeshIndex + 1; i < _subMeshCount; i++)
			{
				_subMeshOffsets[i] = triangleCount;
			}

			this._triangles.Resize(triangleCount);
			triangles = this._triangles.Data;

			dst = 0;
			for (int i = 0; i < vertexCount; i++)
			{
				var vert = vertices[i];
				if (vert.tcount > 0)
				{
					vert.tstart = dst;
					vertices[i] = vert;

					if (dst != i)
					{
						vertices[dst].p = vert.p;
						if (vertNormals != null) vertNormals[dst] = vertNormals[i];
						if (vertTangents != null) vertTangents[dst] = vertTangents[i];
						if (vertUV2D != null)
						{
							for (int j = 0; j < kUVChannelCount; j++)
							{
								var vertUV = vertUV2D[j];
								if (vertUV != null)
								{
									vertUV[dst] = vertUV[i];
								}
							}
						}
						if (vertUV3D != null)
						{
							for (int j = 0; j < kUVChannelCount; j++)
							{
								var vertUV = vertUV3D[j];
								if (vertUV != null)
								{
									vertUV[dst] = vertUV[i];
								}
							}
						}
						if (vertUV4D != null)
						{
							for (int j = 0; j < kUVChannelCount; j++)
							{
								var vertUV = vertUV4D[j];
								if (vertUV != null)
								{
									vertUV[dst] = vertUV[i];
								}
							}
						}
						if (vertColors != null) vertColors[dst] = vertColors[i];
						if (vertBoneWeights != null) vertBoneWeights[dst] = vertBoneWeights[i];
					}
					++dst;
				}
			}

			for (int i = 0; i < triangleCount; i++)
			{
				var triangle = triangles[i];
				triangle.v0 = vertices[triangle.v0].tstart;
				triangle.v1 = vertices[triangle.v1].tstart;
				triangle.v2 = vertices[triangle.v2].tstart;
				triangles[i] = triangle;
			}

			vertexCount = dst;
			this._vertices.Resize(vertexCount);
			if (vertNormals != null) this._vertNormals.Resize(vertexCount, true);
			if (vertTangents != null) this._vertTangents.Resize(vertexCount, true);
			if (vertUV2D != null) this._vertUV2D.Resize(vertexCount, true);
			if (vertUV3D != null) this._vertUV3D.Resize(vertexCount, true);
			if (vertUV4D != null) this._vertUV4D.Resize(vertexCount, true);
			if (vertColors != null) this._vertColors.Resize(vertexCount, true);
			if (vertBoneWeights != null) this._vertBoneWeights.Resize(vertexCount, true);
		}
		#endregion

		#region Calculate Sub Mesh Offsets
		private void CalculateSubMeshOffsets()
		{
			int lastSubMeshIndex = -1;
			_subMeshOffsets = new int[_subMeshCount];

			var triangles = this._triangles.Data;
			int triangleCount = this._triangles.Length;
			for (int i = 0; i < triangleCount; i++)
			{
				var triangle = triangles[i];
				if (triangle.subMeshIndex > lastSubMeshIndex)
				{
					for (int j = lastSubMeshIndex + 1; j < triangle.subMeshIndex; j++)
					{
						_subMeshOffsets[j] = i;
					}
					_subMeshOffsets[triangle.subMeshIndex] = i;
					lastSubMeshIndex = triangle.subMeshIndex;
				}
			}

			for (int i = lastSubMeshIndex + 1; i < _subMeshCount; i++)
			{
				_subMeshOffsets[i] = triangleCount;
			}
		}
		#endregion
		#endregion

		#region Public Methods
		#region Sub-Meshes
		/// <summary>
		/// Returns the triangle indices for a specific sub-mesh.
		/// </summary>
		/// <param name="subMeshIndex">The sub-mesh index.</param>
		/// <returns>The triangle indices.</returns>
		public int[] GetSubMeshTriangles(int subMeshIndex)
		{
			if (subMeshIndex < 0)
				throw new ArgumentOutOfRangeException("subMeshIndex", "The sub-mesh index is negative.");

			// First get the sub-mesh offsets
			if (_subMeshOffsets == null)
			{
				CalculateSubMeshOffsets();
			}

			if (subMeshIndex >= _subMeshOffsets.Length)
				throw new ArgumentOutOfRangeException("subMeshIndex", "The sub-mesh index is greater than or equals to the sub mesh count.");
			else if (_subMeshOffsets.Length != _subMeshCount)
				throw new InvalidOperationException("The sub-mesh triangle offsets array is not the same size as the count of sub-meshes. This should not be possible to happen.");

			var triangles = this._triangles.Data;
			int triangleCount = this._triangles.Length;

			int startOffset = _subMeshOffsets[subMeshIndex];
			if (startOffset >= triangleCount)
				return new int[0];

			int endOffset = ((subMeshIndex + 1) < _subMeshCount ? _subMeshOffsets[subMeshIndex + 1] : triangleCount);
			int subMeshTriangleCount = endOffset - startOffset;
			if (subMeshTriangleCount < 0) subMeshTriangleCount = 0;
			int[] subMeshIndices = new int[subMeshTriangleCount * 3];

			Debug.AssertFormat(startOffset >= 0, "The start sub mesh offset at index {0} was below zero ({1}).", subMeshIndex, startOffset);
			Debug.AssertFormat(endOffset >= 0, "The end sub mesh offset at index {0} was below zero ({1}).", subMeshIndex + 1, endOffset);
			Debug.AssertFormat(startOffset < triangleCount, "The start sub mesh offset at index {0} was higher or equal to the triangle count ({1} >= {2}).", subMeshIndex, startOffset, triangleCount);
			Debug.AssertFormat(endOffset <= triangleCount, "The end sub mesh offset at index {0} was higher than the triangle count ({1} > {2}).", subMeshIndex + 1, endOffset, triangleCount);

			for (int triangleIndex = startOffset; triangleIndex < endOffset; triangleIndex++)
			{
				var triangle = triangles[triangleIndex];
				int offset = (triangleIndex - startOffset) * 3;
				subMeshIndices[offset] = triangle.v0;
				subMeshIndices[offset + 1] = triangle.v1;
				subMeshIndices[offset + 2] = triangle.v2;
			}

			return subMeshIndices;
		}

		/// <summary>
		/// Clears out all sub-meshes.
		/// </summary>
		public void ClearSubMeshes()
		{
			_subMeshCount = 0;
			_subMeshOffsets = null;
			_triangles.Resize(0);
		}

		/// <summary>
		/// Adds a sub-mesh triangle indices for a specific sub-mesh.
		/// </summary>
		/// <param name="triangles">The triangle indices.</param>
		public void AddSubMeshTriangles(int[] triangles)
		{
			if (triangles == null)
				throw new ArgumentNullException("triangles");
			else if ((triangles.Length % 3) != 0)
				throw new ArgumentException("The index array length must be a multiple of 3 in order to represent triangles.", "triangles");

			int subMeshIndex = _subMeshCount++;
			int triangleIndex = this._triangles.Length;
			int subMeshTriangleCount = triangles.Length / 3;
			this._triangles.Resize(this._triangles.Length + subMeshTriangleCount);
			var trisArr = this._triangles.Data;
			for (int i = 0; i < subMeshTriangleCount; i++)
			{
				int offset = i * 3;
				int v0 = triangles[offset];
				int v1 = triangles[offset + 1];
				int v2 = triangles[offset + 2];
				trisArr[triangleIndex + i] = new Triangle(v0, v1, v2, subMeshIndex);
			}

			triangleIndex += subMeshTriangleCount;
		}

		/// <summary>
		/// Adds several sub-meshes at once with their triangle indices for each sub-mesh.
		/// </summary>
		/// <param name="triangles">The triangle indices for each sub-mesh.</param>
		public void AddSubMeshTriangles(int[][] triangles)
		{
			if (triangles == null)
				throw new ArgumentNullException("triangles");

			int totalTriangleCount = 0;
			for (int i = 0; i < triangles.Length; i++)
			{
				if (triangles[i] == null)
					throw new ArgumentException(string.Format("The index array at index {0} is null.", i));
				else if ((triangles[i].Length % 3) != 0)
					throw new ArgumentException(string.Format("The index array length at index {0} must be a multiple of 3 in order to represent triangles.", i), "triangles");

				totalTriangleCount += triangles[i].Length / 3;
			}

			int triangleIndex = this._triangles.Length;
			this._triangles.Resize(this._triangles.Length + totalTriangleCount);
			var trisArr = this._triangles.Data;

			for (int i = 0; i < triangles.Length; i++)
			{
				int subMeshIndex = _subMeshCount++;
				var subMeshTriangles = triangles[i];
				int subMeshTriangleCount = subMeshTriangles.Length / 3;
				for (int j = 0; j < subMeshTriangleCount; j++)
				{
					int offset = j * 3;
					int v0 = subMeshTriangles[offset];
					int v1 = subMeshTriangles[offset + 1];
					int v2 = subMeshTriangles[offset + 2];
					trisArr[triangleIndex + j] = new Triangle(v0, v1, v2, subMeshIndex);
				}

				triangleIndex += subMeshTriangleCount;
			}
		}
		#endregion

		#region UV Sets
		#region Getting
		/// <summary>
		/// Returns the UVs (2D) from a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <returns>The UVs.</returns>
		public Vector2[] GetUVs2D(int channel)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (_vertUV2D != null && _vertUV2D[channel] != null)
			{
				return _vertUV2D[channel].Data;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Returns the UVs (3D) from a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <returns>The UVs.</returns>
		public Vector3[] GetUVs3D(int channel)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (_vertUV3D != null && _vertUV3D[channel] != null)
			{
				return _vertUV3D[channel].Data;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Returns the UVs (4D) from a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <returns>The UVs.</returns>
		public Vector4[] GetUVs4D(int channel)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (_vertUV4D != null && _vertUV4D[channel] != null)
			{
				return _vertUV4D[channel].Data;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Returns the UVs (2D) from a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void GetUVs(int channel, List<Vector2> uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");
			else if (uvs == null)
				throw new ArgumentNullException("uvs");

			uvs.Clear();
			if (_vertUV2D != null && _vertUV2D[channel] != null)
			{
				var uvData = _vertUV2D[channel].Data;
				if (uvData != null)
				{
					uvs.AddRange(uvData);
				}
			}
		}

		/// <summary>
		/// Returns the UVs (3D) from a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void GetUVs(int channel, List<Vector3> uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");
			else if (uvs == null)
				throw new ArgumentNullException("uvs");

			uvs.Clear();
			if (_vertUV3D != null && _vertUV3D[channel] != null)
			{
				var uvData = _vertUV3D[channel].Data;
				if (uvData != null)
				{
					uvs.AddRange(uvData);
				}
			}
		}

		/// <summary>
		/// Returns the UVs (4D) from a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void GetUVs(int channel, List<Vector4> uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");
			else if (uvs == null)
				throw new ArgumentNullException("uvs");

			uvs.Clear();
			if (_vertUV4D != null && _vertUV4D[channel] != null)
			{
				var uvData = _vertUV4D[channel].Data;
				if (uvData != null)
				{
					uvs.AddRange(uvData);
				}
			}
		}
		#endregion

		#region Setting
		/// <summary>
		/// Sets the UVs (2D) for a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void SetUVs(int channel, Vector2[] uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (uvs != null && uvs.Length > 0)
			{
				if (_vertUV2D == null)
					_vertUV2D = new UVChannels<Vector2>();

				int uvCount = uvs.Length;
				var uvSet = _vertUV2D[channel];
				if (uvSet != null)
				{
					uvSet.Resize(uvCount);
				}
				else
				{
					uvSet = new ResizableArray<Vector2>(uvCount, uvCount);
					_vertUV2D[channel] = uvSet;
				}

				var uvData = uvSet.Data;
				uvs.CopyTo(uvData, 0);
			}
			else
			{
				if (_vertUV2D != null)
				{
					_vertUV2D[channel] = null;
				}
			}

			if (_vertUV3D != null)
			{
				_vertUV3D[channel] = null;
			}
			if (_vertUV4D != null)
			{
				_vertUV4D[channel] = null;
			}
		}

		/// <summary>
		/// Sets the UVs (3D) for a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void SetUVs(int channel, Vector3[] uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (uvs != null && uvs.Length > 0)
			{
				if (_vertUV3D == null)
					_vertUV3D = new UVChannels<Vector3>();

				int uvCount = uvs.Length;
				var uvSet = _vertUV3D[channel];
				if (uvSet != null)
				{
					uvSet.Resize(uvCount);
				}
				else
				{
					uvSet = new ResizableArray<Vector3>(uvCount, uvCount);
					_vertUV3D[channel] = uvSet;
				}

				var uvData = uvSet.Data;
				uvs.CopyTo(uvData, 0);
			}
			else
			{
				if (_vertUV3D != null)
				{
					_vertUV3D[channel] = null;
				}
			}

			if (_vertUV2D != null)
			{
				_vertUV2D[channel] = null;
			}
			if (_vertUV4D != null)
			{
				_vertUV4D[channel] = null;
			}
		}

		/// <summary>
		/// Sets the UVs (4D) for a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void SetUVs(int channel, Vector4[] uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (uvs != null && uvs.Length > 0)
			{
				if (_vertUV4D == null)
					_vertUV4D = new UVChannels<Vector4>();

				int uvCount = uvs.Length;
				var uvSet = _vertUV4D[channel];
				if (uvSet != null)
				{
					uvSet.Resize(uvCount);
				}
				else
				{
					uvSet = new ResizableArray<Vector4>(uvCount, uvCount);
					_vertUV4D[channel] = uvSet;
				}

				var uvData = uvSet.Data;
				uvs.CopyTo(uvData, 0);
			}
			else
			{
				if (_vertUV4D != null)
				{
					_vertUV4D[channel] = null;
				}
			}

			if (_vertUV2D != null)
			{
				_vertUV2D[channel] = null;
			}
			if (_vertUV3D != null)
			{
				_vertUV3D[channel] = null;
			}
		}

		/// <summary>
		/// Sets the UVs (2D) for a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void SetUVs(int channel, List<Vector2> uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (uvs != null && uvs.Count > 0)
			{
				if (_vertUV2D == null)
					_vertUV2D = new UVChannels<Vector2>();

				int uvCount = uvs.Count;
				var uvSet = _vertUV2D[channel];
				if (uvSet != null)
				{
					uvSet.Resize(uvCount);
				}
				else
				{
					uvSet = new ResizableArray<Vector2>(uvCount, uvCount);
					_vertUV2D[channel] = uvSet;
				}

				var uvData = uvSet.Data;
				uvs.CopyTo(uvData, 0);
			}
			else
			{
				if (_vertUV2D != null)
				{
					_vertUV2D[channel] = null;
				}
			}

			if (_vertUV3D != null)
			{
				_vertUV3D[channel] = null;
			}
			if (_vertUV4D != null)
			{
				_vertUV4D[channel] = null;
			}
		}

		/// <summary>
		/// Sets the UVs (3D) for a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void SetUVs(int channel, List<Vector3> uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (uvs != null && uvs.Count > 0)
			{
				if (_vertUV3D == null)
					_vertUV3D = new UVChannels<Vector3>();

				int uvCount = uvs.Count;
				var uvSet = _vertUV3D[channel];
				if (uvSet != null)
				{
					uvSet.Resize(uvCount);
				}
				else
				{
					uvSet = new ResizableArray<Vector3>(uvCount, uvCount);
					_vertUV3D[channel] = uvSet;
				}

				var uvData = uvSet.Data;
				uvs.CopyTo(uvData, 0);
			}
			else
			{
				if (_vertUV3D != null)
				{
					_vertUV3D[channel] = null;
				}
			}

			if (_vertUV2D != null)
			{
				_vertUV2D[channel] = null;
			}
			if (_vertUV4D != null)
			{
				_vertUV4D[channel] = null;
			}
		}

		/// <summary>
		/// Sets the UVs (4D) for a specific channel.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <param name="uvs">The UVs.</param>
		public void SetUVs(int channel, List<Vector4> uvs)
		{
			if (channel < 0 || channel >= kUVChannelCount)
				throw new ArgumentOutOfRangeException("channel");

			if (uvs != null && uvs.Count > 0)
			{
				if (_vertUV4D == null)
					_vertUV4D = new UVChannels<Vector4>();

				int uvCount = uvs.Count;
				var uvSet = _vertUV4D[channel];
				if (uvSet != null)
				{
					uvSet.Resize(uvCount);
				}
				else
				{
					uvSet = new ResizableArray<Vector4>(uvCount, uvCount);
					_vertUV4D[channel] = uvSet;
				}

				var uvData = uvSet.Data;
				uvs.CopyTo(uvData, 0);
			}
			else
			{
				if (_vertUV4D != null)
				{
					_vertUV4D[channel] = null;
				}
			}

			if (_vertUV2D != null)
			{
				_vertUV2D[channel] = null;
			}
			if (_vertUV3D != null)
			{
				_vertUV3D[channel] = null;
			}
		}
		#endregion
		#endregion

		#region Initialize
		/// <summary>
		/// Initializes the algorithm with the original mesh.
		/// </summary>
		/// <param name="mesh">The mesh.</param>
		public void Initialize(Mesh mesh)
		{
			if (mesh == null)
				throw new ArgumentNullException("mesh");

			this.Vertices = mesh.vertices;
			this.Normals = mesh.normals;
			this.Tangents = mesh.tangents;
			this.UV1 = mesh.uv;
			this.UV2 = mesh.uv2;
			this.UV3 = mesh.uv3;
			this.UV4 = mesh.uv4;
			this.Colors = mesh.colors;
			this.BoneWeights = mesh.boneWeights;
			this._bindposes = mesh.bindposes;

			ClearSubMeshes();

			int subMeshCount = mesh.subMeshCount;
			int[][] subMeshTriangles = new int[subMeshCount][];
			for (int i = 0; i < subMeshCount; i++)
			{
				subMeshTriangles[i] = mesh.GetTriangles(i);
			}
			AddSubMeshTriangles(subMeshTriangles);
		}
		#endregion

		#region Simplify Mesh
		/// <summary>
		/// Simplifies the mesh to a desired quality.
		/// </summary>
		/// <param name="quality">The target quality (between 0 and 1).</param>
		public void SimplifyMesh(float quality)
		{
			quality = Mathf.Clamp01(quality);

			int deletedTris = 0;
			ResizableArray<bool> deleted0 = new ResizableArray<bool>(20);
			ResizableArray<bool> deleted1 = new ResizableArray<bool>(20);
			Triangle[] triangles = this._triangles.Data;
			int triangleCount = this._triangles.Length;
			int startTrisCount = triangleCount;
			Vertex[] vertices = this._vertices.Data;
			int targetTrisCount = Mathf.RoundToInt(triangleCount * quality);

			for (int iteration = 0; iteration < _maxIterationCount; iteration++)
			{
				if ((startTrisCount - deletedTris) <= targetTrisCount)
					break;

				// Update mesh once in a while
				if ((iteration % 5) == 0)
				{
					UpdateMesh(iteration);
					triangles = this._triangles.Data;
					triangleCount = this._triangles.Length;
					vertices = this._vertices.Data;
				}

				// Clear dirty flag
				for (int i = 0; i < triangleCount; i++)
				{
					triangles[i].dirty = false;
				}

				// All triangles with edges below the threshold will be removed
				//
				// The following numbers works well for most models.
				// If it does not, try to adjust the 3 parameters
				double threshold = 0.000000001 * Math.Pow(iteration + 3, _agressiveness);

				if (_verbose)
				{
					Debug.LogFormat("iteration {0} - triangles {1} threshold {2}", iteration, (startTrisCount - deletedTris), threshold);
				}

				// Remove vertices & mark deleted triangles
				RemoveVertexPass(startTrisCount, targetTrisCount, threshold, deleted0, deleted1, ref deletedTris);
				//ColorizeBorderVertices();
				//break;
			}

			CompactMesh();

			if (_verbose)
			{
				Debug.LogFormat("Finished simplification with triangle count {0}", this._triangles.Length);
			}
		}

		public void ColorizeBorderVertices()
		{
			if(_vertColors == null)
			{
				_vertColors = new ResizableArray<Color>(_vertices.Length);
			}
			for (int i = 0; i < _vertices.Length; i++)
			{
				if(_vertices[i].border)
				{
					_vertColors[i] = Color.red;
				}
			}
		}

		/// <summary>
		/// Simplifies the mesh without losing too much quality.
		/// </summary>
		public void SimplifyMeshLossless()
		{
			int deletedTris = 0;
			ResizableArray<bool> deleted0 = new ResizableArray<bool>(0);
			ResizableArray<bool> deleted1 = new ResizableArray<bool>(0);
			var triangles = this._triangles.Data;
			int triangleCount = this._triangles.Length;
			int startTrisCount = triangleCount;
			var vertices = this._vertices.Data;

			for (int iteration = 0; iteration < 9999; iteration++)
			{
				// Update mesh constantly
				UpdateMesh(iteration);
				triangles = this._triangles.Data;
				triangleCount = this._triangles.Length;
				vertices = this._vertices.Data;

				// Clear dirty flag
				for (int i = 0; i < triangleCount; i++)
				{
					triangles[i].dirty = false;
				}

				// All triangles with edges below the threshold will be removed
				//
				// The following numbers works well for most models.
				// If it does not, try to adjust the 3 parameters
				double threshold = kDoubleEpsilon;

				if (_verbose)
				{
					Debug.LogFormat("Lossless iteration {0} - triangles {1}", iteration, triangleCount);
				}

				// Remove vertices & mark deleted triangles
				RemoveVertexPass(startTrisCount, 0, threshold, deleted0, deleted1, ref deletedTris);

				if (deletedTris <= 0)
					break;

				deletedTris = 0;
			}

			CompactMesh();

			if (_verbose)
			{
				Debug.LogFormat("Finished simplification with triangle count {0}", this._triangles.Length);
			}
		}
		#endregion

		#region To Mesh
		/// <summary>
		/// Returns the resulting mesh.
		/// </summary>
		/// <returns>The resulting mesh.</returns>
		public Mesh ToMesh()
		{
			var vertices = this.Vertices;
			var normals = this.Normals;
			var tangents = this.Tangents;
			var colors = this.Colors;
			var boneWeights = this.BoneWeights;

			var newMesh = new Mesh();

#if UNITY_2017_3 || UNITY_2017_4 || UNITY_2018
			// TODO: Use baseVertex if all submeshes are within the ushort.MaxValue range even though the total vertex count is above
			bool use32BitIndex = (vertices.Length > ushort.MaxValue);
			newMesh.indexFormat = (use32BitIndex ? UnityEngine.Rendering.IndexFormat.UInt32 : UnityEngine.Rendering.IndexFormat.UInt16);
#endif

			if (_bindposes != null && _bindposes.Length > 0)
			{
				newMesh.bindposes = _bindposes;
			}

			newMesh.subMeshCount = _subMeshCount;
			newMesh.vertices = this.Vertices;
			if (normals != null) newMesh.normals = normals;
			if (tangents != null) newMesh.tangents = tangents;

			if (_vertUV2D != null)
			{
				List<Vector2> uvSet = null;
				for (int i = 0; i < kUVChannelCount; i++)
				{
					if (_vertUV2D[i] != null)
					{
						if (uvSet == null)
							uvSet = new List<Vector2>(_vertUV2D[i].Length);

						GetUVs(i, uvSet);
						newMesh.SetUVs(i, uvSet);
					}
				}
			}

			if (_vertUV3D != null)
			{
				List<Vector3> uvSet = null;
				for (int i = 0; i < kUVChannelCount; i++)
				{
					if (_vertUV3D[i] != null)
					{
						if (uvSet == null)
							uvSet = new List<Vector3>(_vertUV3D[i].Length);

						GetUVs(i, uvSet);
						newMesh.SetUVs(i, uvSet);
					}
				}
			}

			if (_vertUV4D != null)
			{
				List<Vector4> uvSet = null;
				for (int i = 0; i < kUVChannelCount; i++)
				{
					if (_vertUV4D[i] != null)
					{
						if (uvSet == null)
							uvSet = new List<Vector4>(_vertUV4D[i].Length);

						GetUVs(i, uvSet);
						newMesh.SetUVs(i, uvSet);
					}
				}
			}

			if (colors != null) newMesh.colors = colors;
			if (boneWeights != null) newMesh.boneWeights = boneWeights;

			for (int i = 0; i < _subMeshCount; i++)
			{
				var subMeshTriangles = GetSubMeshTriangles(i);
				newMesh.SetTriangles(subMeshTriangles, i, false);
			}

			if (_enableSmartLink)
			{
				SplitCorespondingVertices(newMesh);
			}
			else
			{
				newMesh.RecalculateBounds();
			}
			return newMesh;
		}

		public static void SplitCorespondingVertices(Mesh mesh)
		{
			int newVertIndex = 0;
			int subMeshCount = mesh.subMeshCount;

			Vector3[] originalVerts = mesh.vertices;
			Color[] originalColors = mesh.colors;
			Vector2[] originalUV = mesh.uv;
			Vector2[] originalUV2 = mesh.uv2;
			Vector2[] originalUV3 = mesh.uv3;
			Vector2[] originalUV4 = mesh.uv4;

			int[][] subMeshTriangles = new int[subMeshCount][];

			List<Vector3> newVerts = new List<Vector3>(mesh.vertexCount);
			List<Vector3> newNormals = new List<Vector3>(mesh.vertexCount);
			List<Color> newColors = null;
			if (originalColors != null && originalColors.Length > 0) { newColors = new List<Color>(mesh.vertexCount); }
			List<Vector2> newUV = null;
			if (originalUV != null && originalUV.Length > 0) { newUV = new List<Vector2>(mesh.vertexCount); }
			List<Vector2> newUV2 = null;
			if (originalUV2 != null && originalUV2.Length > 0) { newUV2 = new List<Vector2>(mesh.vertexCount); }
			List<Vector2> newUV3 = null;
			if (originalUV3 != null && originalUV3.Length > 0) { newUV3 = new List<Vector2>(mesh.vertexCount); }
			List<Vector2> newUV4 = null;
			if (originalUV4 != null && originalUV4.Length > 0) { newUV4 = new List<Vector2>(mesh.vertexCount); }

			for (int i = 0; i < subMeshCount; i++)
			{
				subMeshTriangles[i] = mesh.GetTriangles(i);

				for (int j = 0; j < subMeshTriangles[i].Length; j += 3)
				{
					int vertexId = subMeshTriangles[i][j];
					int colorId = subMeshTriangles[i][j];
					newVerts.Add(originalVerts[vertexId]);
					if (newColors != null) { newColors.Add(originalColors[colorId]); }
					if (newUV != null) { newUV.Add(originalUV[vertexId]); }
					if (newUV2 != null) { newUV2.Add(originalUV2[vertexId]); }
					if (newUV3 != null) { newUV3.Add(originalUV3[vertexId]); }
					if (newUV4 != null) { newUV4.Add(originalUV4[vertexId]); }
					subMeshTriangles[i][j] = newVertIndex;
					++newVertIndex;

					vertexId = subMeshTriangles[i][j + 1];
					newVerts.Add(originalVerts[vertexId]);
					if (newColors != null) { newColors.Add(originalColors[colorId]); }
					if (newUV != null) { newUV.Add(originalUV[vertexId]); }
					if (newUV2 != null) { newUV2.Add(originalUV2[vertexId]); }
					if (newUV3 != null) { newUV3.Add(originalUV3[vertexId]); }
					if (newUV4 != null) { newUV4.Add(originalUV4[vertexId]); }
					subMeshTriangles[i][j + 1] = newVertIndex;
					++newVertIndex;

					vertexId = subMeshTriangles[i][j + 2];
					newVerts.Add(originalVerts[vertexId]);
					if (newColors != null) { newColors.Add(originalColors[colorId]); }
					if (newUV != null) { newUV.Add(originalUV[vertexId]); }
					if (newUV2 != null) { newUV2.Add(originalUV2[vertexId]); }
					if (newUV3 != null) { newUV3.Add(originalUV3[vertexId]); }
					if (newUV4 != null) { newUV4.Add(originalUV4[vertexId]); }
					subMeshTriangles[i][j + 2] = newVertIndex;
					++newVertIndex;

					Vector3 normal = Vector3.Cross(newVerts[newVertIndex - 1] - newVerts[newVertIndex - 2], newVerts[newVertIndex - 3] - newVerts[newVertIndex - 2]).normalized;
					newNormals.Add(normal);
					newNormals.Add(normal);
					newNormals.Add(normal);
				}
			}

			mesh.Clear(false);
			mesh.SetVertices(newVerts);
			mesh.SetNormals(newNormals);
			mesh.SetColors(newColors);
			mesh.SetUVs(0, newUV);
			mesh.SetUVs(1, newUV2);
			mesh.SetUVs(2, newUV3);
			mesh.SetUVs(3, newUV4);
			for (int i = 0; i < subMeshCount; i++)
			{
				mesh.SetTriangles(subMeshTriangles[i], i, false);
			}
			mesh.RecalculateBounds();
		}
		#endregion
		#endregion
	}
}